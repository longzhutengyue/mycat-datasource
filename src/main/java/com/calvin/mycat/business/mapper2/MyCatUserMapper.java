package com.calvin.mycat.business.mapper2;

import com.calvin.mycat.business.domain.User;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Title MyCatUserMapper
 * @Description
 * @author calvin
 * @date: 2019/11/27 11:03 AM 
 */

@Mapper
public interface MyCatUserMapper {

    @Insert("insert into user(id,name) value (#{id},#{name})")
    int insert(User user);

    @Select("select * from user")
    List<User> selectAll();

}
