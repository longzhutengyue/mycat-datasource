package com.calvin.mycat.business.controller;

import com.calvin.mycat.business.domain.User;
import com.calvin.mycat.business.mapper2.MyCatUserMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title MyCatUserController
 * @Description
 * @author calvin
 * @date: 2019/11/27 11:06 AM 
 */

@RestController
@RequestMapping("/mycat/user")
public class MyCatUserController {

    @Autowired
    private MyCatUserMapper myCatUserMapper;

    @RequestMapping("/save")
    public String save(User user) {
        myCatUserMapper.insert(user);
        return "保存成功";
    }

    @RequestMapping("/list")
    public List<User> list() {
        return myCatUserMapper.selectAll();
    }

}
