package com.calvin.mycat.business.controller;

import com.calvin.mycat.business.domain.User;
import com.calvin.mycat.business.mapper1.CommonUserMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title CommonUserController
 * @Description
 * @author calvin
 * @date: 2019/11/27 11:08 AM 
 */

@RestController
@RequestMapping("/common/user")
public class CommonUserController {

    @Autowired
    private CommonUserMapper commonUserMapper;

    @RequestMapping("/save")
    public String save(User user) {
        commonUserMapper.insert(user);
        return "保存成功";
    }

    @RequestMapping("/list")
    public List<User> list() {
        return commonUserMapper.selectAll();
    }

}
