package com.calvin.mycat.business.service.impl;

import com.calvin.mycat.business.domain.User;
import com.calvin.mycat.business.service.MyCatUserService;
import org.springframework.stereotype.Service;

/**
 * @Title MyCatUserServiceImpl
 * @Description mycat库的用户接口实现类
 * @author calvin
 * @date: 2019/11/27 10:54 AM 
 */

@Service
public class MyCatUserServiceImpl implements MyCatUserService {

    /**
     * 创建用户
     * @param user
     * @return
     */
    @Override
    public String save(User user) {
        return null;
    }
}
