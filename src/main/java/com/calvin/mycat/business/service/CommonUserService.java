package com.calvin.mycat.business.service;

import com.calvin.mycat.business.domain.User;

/**
 * @Title CommonUserService
 * @Description 普通库的用户接口
 * @author calvin
 * @date: 2019/11/27 10:49 AM 
 */
public interface CommonUserService {

    /**
     * 创建用户
     * @param user
     * @return
     */
    String save(User user);

}
