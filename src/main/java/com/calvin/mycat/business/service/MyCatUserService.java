package com.calvin.mycat.business.service;

import com.calvin.mycat.business.domain.User;

/**
 * @Title MyCatUserService
 * @Description mycat库的用户接口
 * @author calvin
 * @date: 2019/11/27 10:50 AM 
 */
public interface MyCatUserService {

    /**
     * 创建用户
     * @param user
     * @return
     */
    String save(User user);

}
