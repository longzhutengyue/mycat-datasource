package com.calvin.mycat.business.mapper1;

import com.calvin.mycat.business.domain.User;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Title CommonUserMapper
 * @Description
 * @author calvin
 * @date: 2019/11/27 11:04 AM 
 */

@Mapper
public interface CommonUserMapper {

    @Insert("insert into user(id,name) value (#{id},#{name})")
    int insert(User user);

    @Select("select * from user")
    List<User> selectAll();

}
