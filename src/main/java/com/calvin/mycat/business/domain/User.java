package com.calvin.mycat.business.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @Title User
 * @Description 定义实体类
 * @author calvin
 * @date: 2019/11/27 10:48 AM 
 */

@Setter
@Getter
public class User {

    private Long id;

    private String name;

}
