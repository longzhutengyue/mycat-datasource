package com.calvin.mycat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycatDatasourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MycatDatasourceApplication.class, args);
	}

}
